package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class ShadRiley_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> AllTracks = new ArrayList<Song>();		//Array AllTracks to store all songs
    
	//Toto songs
	Toto Toto = new Toto();
    AllTracks = Toto.getTotoSongs();
	playlist.add(AllTracks.get(0));							//Play track 1 of Toto
	playlist.add(AllTracks.get(1));							//Play track 2 of Toto
	
	//Kenny Loggins songs
	KennyLoggins KennyLoggins = new KennyLoggins();
    AllTracks = KennyLoggins.getKennyLogginsSongs();
	playlist.add(AllTracks.get(0));							//Play track 1 of Kenny Loggins
	playlist.add(AllTracks.get(1));							//Play track 2 of Kenny Loggins
	
	//The Beatles songs
	TheBeatles Beatles = new TheBeatles();
    AllTracks = Beatles.getBeatlesSongs();					
	playlist.add(AllTracks.get(2));							//Play track 3 of The Beatles
	
	//Guns N' Roses songs
	GunsNRoses GunsNRoses = new GunsNRoses();
	AllTracks = GunsNRoses.getGunsNRosesSongs();
	playlist.add(AllTracks.get(1));							//Play track 2 of Guns N' Roses
	playlist.add(AllTracks.get(2));							//Play track 3 of Guns N' Roses
	
	//Imagine Dragons songs
	ImagineDragons ImagineDragons = new ImagineDragons();
	AllTracks = ImagineDragons.getImagineDragonsSongs();
	playlist.add(AllTracks.get(0));							//Play track 1 of Imagine Dragons
		
    return playlist;
	}
}
