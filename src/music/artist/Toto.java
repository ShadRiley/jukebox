package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Toto {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Toto() {
    }
    
    public ArrayList<Song> getTotoSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Africa", "Toto");              		   		//Create a song
         Song track2 = new Song("Hold the Line", "Toto");       		   		//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list
         this.albumTracks.add(track2);                                          //Add the second song to song list 
         return albumTracks;                                                    //Return the songs for artist or band in the form of an ArrayList
    }
}
