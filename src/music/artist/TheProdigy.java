package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class TheProdigy {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public TheProdigy() {
    }
    
    public ArrayList<Song> getProdigySongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("No Tourists", "The Prodigy");             		//Create a song
         Song track2 = new Song("Boom Boom Tap", "The Prodigy");         		//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Prodigy
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Prodigy 
         return albumTracks;                                                    //Return the songs for the Prodigy in the form of an ArrayList
    }
}
